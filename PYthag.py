def hyp(perplength):
    sidea, sideb = map(float, perplength.split(' '))  # splits the input by a space

    length = (sidea ** 2 + sideb ** 2)  # squares and adds the lengths

    return round(length ** 0.5, 4)  # returns the value rounded to 4 places


perplength = input("What are the perpendicular lengths of the triangles?")

inter = f"the hypotenuse is {hyp(perplength)} units in length"

print(inter)
