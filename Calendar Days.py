class Date:
    def __init__(self, d, m, y):
        self.d = d
        self.m = m
        self.year = y


monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


# Counts number of leap years
def countLeapYears(d):
    years = d.year

    # Check if the current year is a relevant leap year
    if (d.m <= 2):
        years -= 1

    return int(years / 4 - years / 100 + years / 400)


def get_difference(date_one, date_two):
    # count using years and day
    n1 = date_one.year * 365 + date_one.d

    # Add days for months in date
    for i in range(0, date_one.m - 1):
        n1 += monthDays[i]

    # Add a day for every leap year
    n1 += countLeapYears(date_one)

    # Days before 'dt2'

    n2 = date_two.year * 365 + date_two.d
    for i in range(0, date_two.m - 1):
        n2 += monthDays[i]
    n2 += countLeapYears(date_two)

    # return difference between two counts
    return n2 - n1



date_one = Date(1, 1, 1970)
date_two = Date(1, 1, 1970)

print("Please input dates in format DD-MM-YYYY")

date1 = input("What's the first date?")
date2 = input("What's the second date?")

date_one.d, date_one.m, date_one.year = map(int, date1.split('-'))
date_two.d, date_two.m, date_two.year = map(int, date2.split('-'))

print(get_difference(date_one, date_two), "days")


if __name__ == "__main__":
    main()
