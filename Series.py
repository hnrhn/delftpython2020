import math


def funca(n):
    stepone = (math.pow(2, n) * math.factorial(n)) / math.pow(n, n) # this is the equation
    if n > 142:
        """" 
    Only goes up to n = 143 because beyond that Python stops dealing with it,
    presumably because the differences are minuscule 
    """
        return 0 # ends the recursion here by not calling the function again
    else:
        return stepone + funca(n + 1) # pure recursion, calls the function again


def funcb(m):
    equation = (math.pow(5, m) + math.pow(4, m)) / math.pow(6, m)
    if m > 142:
        return 0
    else:
        return equation + funcb(m + 1)


def funcc(n):
    eqat = math.pow(-1,n) * (math.pow(math.sin(n),2)) / n
    if n > 990:
        return 0
    else:
        return eqat + funcc(n+1)


def funcd(n):
    eq = math.pow(-1,n) * ((2*n) +1)/((3*n)+2)
    if n > 250:
        return 0
    else:
        return eq + funcd(n+1)


def funce(n):
    uegh = math.pow(-1,n) / math.factorial(n)
    if n > 165:
        return 0
    else:
        return uegh + funce(n+1)


n = 1
m = 0
print(round(funca(n), 3))
print(round(funcb(m), 3))
print(round(funcc(n), 5))
print(round(funcd(n), 3))
print(round(funce(n), 3))
