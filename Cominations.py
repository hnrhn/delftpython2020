import math  # to be used with the checksum


def dumfac(k):  # the manual way of finding the factorial
    if k == 0:
        return 1  # 0! is 1
    elif k < 0:
        return 0  # negative factorials aren't a thing
    elif k == 1:
        return k  # 1 factorial is 1
    else:
        return k * dumfac(k - 1) # Multiplies k by the factorial of k-1, pure recursion


def topfac(i):
    if i == 0:
        return 1
    elif i < 0:
        return 1
    elif i > n - k:  # checks to see if the input is greater than the end point of the factorial
        return i * topfac(i - 1)
    else:
        return 1  # if the input is at the endpoint it only multiplies by 1 and ends the recursion


print("The formula is n over k so;")
k = int(input("What's K?"))
n = int(input("What's N?"))
i = n

value = int(math.factorial(n) / (math.factorial(k) * math.factorial(n - k)))
result = int(topfac(i) / dumfac(k))
checksum = f"as a checksum; the result should be {value}"
print(result)
print(checksum)
