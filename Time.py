def clock_angle(time):
    hour, minutes = map(int, time.split(':'))  # splits the time input into hours and minutes
    angle = abs(30 * (hour % 12) - 5.5 * minutes)  # 30deg per hour, 0.5 deg per minute, then 6deg for minute hand
    return min(angle, 360 - angle)  # return the smaller of the two angles


time = input("What time is it?")

intermediary = f"At {time} the angle between the hands is: {clock_angle(time)} degrees."
print(intermediary)

