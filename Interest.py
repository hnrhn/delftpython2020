import math  # to do funky maths stuff (exponents and logs)
def double (decrate):
    dtime = math.log(2,decrate)# interest rate to the power of what results in 2?
    return round(dtime,3)

# get input data
startdebt = float(input("What's the starting value of the debt?"))
rate = float(input("What's the annual interest rate in %?"))
time = float(input("How many years?"))

decrate = (rate / 100 + 1)# get interest rate in decimal format

value = startdebt * math.pow(decrate, time)# get total rate compounded over the years, then multiply by starting value
round(value,2)# money only exists in 2 decimals

inter = f"At the end of the period the sum would be {value}"
intertime = f"The sum will have doubled after {double(decrate)} years"
print(inter)
print(intertime)


