import os

if not os.path.exists('animals.dat'):
    print("Hold your onions! There appears to have been a problem")
    f = open('start.dat', 'r')
    rowAmount = f.read().splitlines()

else:
    f = open('animals.dat', 'r')
    rowAmount = f.read().splitlines()

table = []
i = 0

while i < len(rowAmount):
    line = rowAmount[i]
    column = line.split(" -- ")
    table.append(column)
    i = i + 1

print(table)

k = 0  # where k is the question index number

while k <= len(rowAmount):
    answer = input(table[k][1] + "?: ").lower()

    if answer == "yes" or answer == "y" or answer == "yeah":
        if not table[k][2].isnumeric():
            answer = input("Final guess, is it a " + table[k][2] + "?: ")
            if answer == "yes" or answer == "y" or answer == "yeah":
                print("Cool!")
                break
            else:
                print("That's a shame.")
                newAnimal = input("What was the correct animal?  ")
                newQuestion = input(f"Give a yes/no question to distinguish {newAnimal} from {table[k][2]}:  ")
                newAnswer = input(f"What was the answer for {newAnimal}?  ")


                # Need to update the table by adding a row and changing the action in the previous line
                if newAnswer.lower() == "yes" or newAnswer.lower() == "y" or newAnswer.lower() == "yeah":
                    newEntry = [str(len(rowAmount)), newQuestion, newAnimal, table[k][2]]
                else:
                    newEntry = [str(len(rowAmount)), newQuestion, table[k][2], newAnimal]

                table[k][2] = str(len(rowAmount))

                table.append(newEntry)

                # rewrite the whole table to animals.dat
                with open('animals.dat', 'w') as fout:

                    j = 0
                    while j <= len(rowAmount):
                        fout.write(" -- ".join(table[j]) + "\n")
                        j = j + 1

                break


        else:
            k = int(table[k][2])


    else:
        if not table[k][3].isnumeric():
            answer = input("Final guess, is it a " + table[k][3] + "?: ")
            if answer == "yes" or answer == "y" or answer == "yeah":
                print("Cool!")
                break
            else:
                print("That's a shame.")
                newAnimal = input("What was the correct animal?  ")
                newQuestion = input(f"Give a yes/no question to distinguish {newAnimal} from {table[k][3]}:  ")
                newAnswer = input(f"What was the answer for {newAnimal}?  ")

                # Need to update the table by adding a row and changing the action in the previous line
                if newAnswer.lower() == "yes" or newAnswer.lower() == "y" or newAnswer.lower() == "yeah":
                    newEntry = [str(len(rowAmount)), newQuestion, newAnimal, table[k][3]]
                else:
                    newEntry = [str(len(rowAmount)), newQuestion, table[k][3], newAnimal]

                table[k][3] = str(len(rowAmount))

                table.append(newEntry)

                # rewrite the whole table to animals.dat
                with open('animals.dat', 'w') as fout:

                    j = 0
                    while j <= len(rowAmount):
                        fout.write(" -- ".join(table[j]) + "\n")
                        j = j + 1

                break

        else:
            k = int(table[k][3])

print(table)
